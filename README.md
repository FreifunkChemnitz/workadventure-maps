# Freifunk Chemnitz 2D Map

This repository contains the 2D variant of the Freifunk office. This can be entered via the following link: https://play.workadventu.re/_/global/newton.chemnitz.freifunk.net/~txt/clt/buero.json

![buero](buero.png)

for licenses see COPYRIGHT file
